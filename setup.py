from distutils.core import setup

setup(
    name='jsonify-lsf',
    version='1.1.1',
    packages=['jsonify_lsf'],
    license='BSD',
    long_description="LSF log to JSON transpiler",
    scripts=["jsonify-lsf"]
)
