# jsonify-lsf

Module and command line script to parse LSF logs into JSON.

## Installation

python setup.py install

Requires the [official Platform LSF Python bindings](https://github.com/PlatformLSF/platform-python-lsf-api/tree/master/pythonlsf).

Note: Can be installed via Puppet with sth. like:
     ensure_packages(['jsonify-lsf', 
                      'LSF-GLIBC-2.3-lib',
                      'LSF-GLIBC-2.3-devel',
                      'platform-python-lsf-api'])

## Usage

jsonify-lsf works with streams. The input stream can be a native Python stream or the official LSF API stream. 

```
from pythonlsf import lsf
import jsonify_lsf 

input = open('/path/to/lsb.events', 'r')
# or input = lsf.fopen('/path/to/lsb.events', 'r')
output = open('/path/to/output.json, 'w')

jsonify_lsf.transform(input, output)
```

> If using a native Python stream for the input, the name attribute is used to safely create an lsf.fopen stream, as the LSF bindings will segfault if given an invalid file path. This allows the module to be handled with sane Python IOErrors. Use the official bindings at your own risk. 