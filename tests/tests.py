import json

from pythonlsf import lsf

import jsonify_lsf


class DummyStream(object):
    def __init__(self):
        self.lines = []

    def write(self, line):
        self.lines.append(line)

    def close(self):
        pass


def test_native_open():
    log = open('partial_lsf_log', 'r')
    out = DummyStream()

    jsonify_lsf.transform(log, out)

    obj = json.loads(out.lines[0])

    assert obj['jobId'] == 787081803


def test_lsf_fopen():
    log = lsf.fopen('partial_lsf_log', 'r')
    out = DummyStream()

    jsonify_lsf.transform(log, out)

    obj = json.loads(out.lines[0])

    assert obj['jobId'] == 787081803
