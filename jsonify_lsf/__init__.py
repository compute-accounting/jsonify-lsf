import json

from pythonlsf import lsf


JUNK_KEYS = (
    'additionalInfo',
    'commandSpool',
    'cpi',
    'cwd',
    'dependCond',
    'effectiveResReq',
    'errFile',
    'exceptMask',
    'flow_id',
    'gbs',
    'gflops',
    'gips',
    'inFile',
    'inFileSpool',
    'jgroup',
    'jStatus',
    'lastResizeTime',
    'licenseProject',
    'loginShell',
    'mailUser',
    'maxRSwap',
    'notifyCmd',
    'numhRusages',
    'options',
    'options2',
    'options3',
    'outdir',
    'outFile',
    'preExecCmd',
    'postExecCmd',
    'requeueEValues',
    'rsvId',
    'runLimit',
    'runtimeEstimation',
    'serial_job_energy',
    'sla',
    'subcwd',
    'timeEvent',
    'warningAction',
    'warningTimePeriod'
)


class Transformer(object):
    def __init__(self, stream_in, stream_out):
        """
        :param stream_in: A file instance, or a file stream from the LSF platform API. Be careful with that, though.
        :type stream_out: file
        """
        self.stream_in = stream_in
        self.stream_out = stream_out

    def event_stream(self):
        """
        lsf.fopen isn't a really good stream handler.
        It will segfault if it points to a file that does not exist,
        hence the init of this function opening a presumably valid
        file stream and using the name.
        """
        if isinstance(self.stream_in, file):
            lsf_stream = lsf.fopen(self.stream_in.name, 'r')
        else:
            lsf_stream = self.stream_in

        line_num = lsf.new_intp()
        lsf.intp_assign(line_num, 0)

        while True:
            event = lsf.lsb_geteventrec(lsf_stream, line_num)

            if event:
                yield event
            else:
                return

    def parse(self):
        for event in self.event_stream():
            self.stream_out.write(json.dumps(event_to_dict(event), ensure_ascii=False) + "\n")

        if isinstance(self.stream_in, file):
            self.stream_in.close()

        self.stream_out.close()


def transform(stream_in, stream_out):
    Transformer(stream_in, stream_out).parse()


def event_to_dict(record):
    """
    Uses reflection to parse an LSF job finish record into a dict.
    :param record: LSF record
    :rtype: dict
    """
    log = record.eventLog.jobFinishLog

    # Filter Python and Swig keys not related to the record
    keys = filter(lambda key: key[:2] != '__' and key != 'this' and key not in JUNK_KEYS, dir(log))

    doc = {}

    for key in keys:
        attr = getattr(log, key)

        if not isinstance(attr, (int, str, long, float)):
            # Some of the values within this struct are not serializable.
            # Ideally, we should parse certain values like execHosts into lists.
            continue

        doc[key] = attr

    return doc
